<?php
/**
 * Taxa list SQL module for Craft CMS 3.x
 *
 * Outputs a list of taxa for the search page
 *
 * @link      nye@dalrymple.eu
 * @copyright Copyright (c) 2018 Nye Hughes
 */

namespace modules\taxalistsqlmodule\assetbundles\TaxaListSqlModule;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * TaxaListSqlModuleAsset AssetBundle
 *
 * AssetBundle represents a collection of asset files, such as CSS, JS, images.
 *
 * Each asset bundle has a unique name that globally identifies it among all asset bundles used in an application.
 * The name is the [fully qualified class name](http://php.net/manual/en/language.namespaces.rules.php)
 * of the class representing it.
 *
 * An asset bundle can depend on other asset bundles. When registering an asset bundle
 * with a view, all its dependent asset bundles will be automatically registered.
 *
 * http://www.yiiframework.com/doc-2.0/guide-structure-assets.html
 *
 * @author    Nye Hughes
 * @package   TaxaListSqlModule
 * @since     1.0.0
 */
class TaxaListSqlModuleAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * Initializes the bundle.
     */
    public function init()
    {
        // define the path that your publishable resources live
        $this->sourcePath = "@modules/taxalistsqlmodule/assetbundles/taxalistsqlmodule/dist";

        // define the dependencies
        $this->depends = [
            CpAsset::class,
        ];

        // define the relative path to CSS/JS files that should be registered with the page
        // when this asset bundle is registered
        $this->js = [
            'js/TaxaListSqlModule.js',
        ];

        $this->css = [
            'css/TaxaListSqlModule.css',
        ];

        parent::init();
    }
}
