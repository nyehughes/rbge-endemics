/**
 * Taxa list SQL module for Craft CMS
 *
 * Taxa list SQL JS
 *
 * @author    Nye Hughes
 * @copyright Copyright (c) 2018 Nye Hughes
 * @link      nye@dalrymple.eu
 * @package   TaxaListSqlModule
 * @since     1.0.0
 */
