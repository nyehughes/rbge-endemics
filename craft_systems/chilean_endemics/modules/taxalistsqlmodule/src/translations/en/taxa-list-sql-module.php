<?php
/**
 * Taxa list SQL module for Craft CMS 3.x
 *
 * Outputs a list of taxa for the search page
 *
 * @link      nye@dalrymple.eu
 * @copyright Copyright (c) 2018 Nye Hughes
 */

/**
 * Taxa list SQL en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('taxa-list-sql-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Nye Hughes
 * @package   TaxaListSqlModule
 * @since     1.0.0
 */
return [
    'Taxa list SQL plugin loaded' => 'Taxa list SQL plugin loaded',
];
