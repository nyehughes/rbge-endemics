<?php
/**
 * Taxa list SQL module for Craft CMS 3.x
 *
 * Outputs a list of taxa for the search page
 *
 * @link      nye@dalrymple.eu
 * @copyright Copyright (c) 2018 Nye Hughes
 */

namespace modules\taxalistsqlmodule\services;

use modules\taxalistsqlmodule\TaxaListSqlModule;

use Craft;
use craft\base\Component;
use craft\elements\Entry;

/**
 * TaxaListSql Service
 *
 * All of your module’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other modules can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Nye Hughes
 * @package   TaxaListSqlModule
 * @since     1.0.0
 */
class TaxaListSql extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin/module file, call it like this:
     *
     *     TaxaListSqlModule::$instance->taxaListSql->exampleService()
     *
     * @return mixed
     */
    
    public function getList()
    {
    
     $sql = <<<EOT
       SELECT DISTINCT `content`.`title`,`elements`.`id` AS taxaID, `elements_sites`.`slug`, `elements_sites`.`uri` AS url, `content`.`field_species` AS species, `genus`.`title` AS genus, lower(`genus`.`title`) AS genusSlug, `genus`.`elementId` AS genusId, `family`.`title` AS familyTitle, lower(`family`.`title`) AS familySlug, `content`.`field_infraspecificAuthor`, `content`.`field_infraspecificEpithet`, `content`.`field_infraspecificStatus`, `content`.`field_isASynonym`, IF(`content`.`field_isASynonym` = '1', 'synonym' , 'accepted') AS synClass, SUBSTRING(`content`.`title`, 1, 1) AS letter,
       
        (SELECT DISTINCT GROUP_CONCAT(DISTINCT catElementsSites.slug SEPARATOR ' ') 
                                                                                      FROM `elements_sites` `catElementsSites`, `relations` `regionsRelation` 
                                                                                           WHERE ((`regionsRelation`.`sourceId` = `elements`.`id`) AND (`regionsRelation`.`fieldId`='56')) 
                                                                                           AND `catElementsSites`.`elementId` = `regionsRelation`.`targetId`)
                                                                                           AS regions,
                                                                                            
        (SELECT DISTINCT GROUP_CONCAT(DISTINCT catElementsSites.slug SEPARATOR ' ') 
                                                                          FROM `elements_sites` `catElementsSites`, `relations` `regionsRelation` 
                                                                               WHERE ((`regionsRelation`.`sourceId` = `elements`.`id`) AND (`regionsRelation`.`fieldId`='78')) 
                                                                               AND `catElementsSites`.`elementId` = `regionsRelation`.`targetId`)
                                                                               AS plantForms, 
                                                                               
        (SELECT DISTINCT IFNULL(GROUP_CONCAT(DISTINCT catElementsSites.slug SEPARATOR ' '), 'not-in-collection')  
                                                                          FROM `elements_sites` `catElementsSites`, `relations` `regionsRelation` 
                                                                               WHERE ((`regionsRelation`.`sourceId` = `elements`.`id`) AND (`regionsRelation`.`fieldId`='81')) 
                                                                               AND `catElementsSites`.`elementId` = `regionsRelation`.`targetId`)
                                                                               AS collections                                                                                                                                                                      
       
       FROM `elements` `elements`
       INNER JOIN `entries` `entries` ON `entries`.`id` = `elements`.`id`
       INNER JOIN `elements_sites` `elements_sites` ON `elements_sites`.`elementId` = `elements`.`id`
       INNER JOIN `content` `content` ON `content`.`elementId` = `elements`.`id`
       LEFT JOIN `relations` `genusRelation` ON (`genusRelation`.`sourceId` = `elements`.`id`) AND (`genusRelation`.`fieldId`='55')
       LEFT JOIN `content` `genus` ON (`genus`.`elementId` = `genusRelation`.`targetId`)
       LEFT JOIN `relations` `familyRelation` ON (`familyRelation`.`sourceId` = `genus`.`elementId`) AND (`familyRelation`.`fieldId`='33')
       LEFT JOIN `content` `family` ON (`family`.`elementId` = `familyRelation`.`targetId`)
       WHERE (`entries`.`sectionId` = '1') AND (`elements_sites`.`siteId`='1') AND (`content`.`siteId`='1') AND (`elements_sites`.`enabled` ='1')
       GROUP BY `title`
       
EOT;
      $list = Craft::$app->db->createCommand($sql)->queryAll();
            
      $result =  $list;
    
        return $result;
    }
    
    
    }
