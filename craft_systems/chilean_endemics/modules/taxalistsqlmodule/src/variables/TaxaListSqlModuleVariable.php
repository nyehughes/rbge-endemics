<?php
/**
 * Taxa list SQL module for Craft CMS 3.x
 *
 * Outputs a list of taxa for the search page
 *
 * @link      nye@dalrymple.eu
 * @copyright Copyright (c) 2018 Nye Hughes
 */

namespace modules\taxalistsqlmodule\variables;

use modules\taxalistsqlmodule\TaxaListSqlModule;

use Craft;

/**
 * Taxa list SQL Variable
 *
 * Craft allows modules to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.taxaListSqlModule }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Nye Hughes
 * @package   TaxaListSqlModule
 * @since     1.0.0
 */
class TaxaListSqlModuleVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.taxaListSqlModule.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.taxaListSqlModule.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {   
            
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
    
    public function getList($optional = null)
    {
    $result = TaxaListSqlModule::getInstance()->taxaListSql->getList();
    return $result;
    }
    
    public function setCategory($categoryFieldHandle = null, $categoryId = null, $sectionName = null)
    {   
        TaxaListSqlModule::getInstance()->taxaListSql->actionSetCategory($categoryFieldHandle, $categoryId, $sectionName);    
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
    
    
}
