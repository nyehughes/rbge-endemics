<?php

$vendorDir = dirname(__DIR__);
$rootDir = dirname(dirname(__DIR__));

return array (
  'verbb/feed-me-pro' => 
  array (
    'class' => 'verbb\\feedmepro\\FeedMePro',
    'basePath' => $vendorDir . '/verbb/feed-me-pro/src',
    'handle' => 'feed-me-pro',
    'aliases' => 
    array (
      '@verbb/feedmepro' => $vendorDir . '/verbb/feed-me-pro/src',
    ),
    'name' => 'Feed Me Pro',
    'version' => '3.0.4',
    'schemaVersion' => '1.0.0',
    'description' => 'Import content from XML, RSS, CSV or JSON feeds into Entries, Categories, Craft Commerce Products (and variants), and more.',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/feed-me',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/feed-me-pro/craft-3/CHANGELOG.md',
  ),
  'doublesecretagency/craft-smartmap' => 
  array (
    'class' => 'doublesecretagency\\smartmap\\SmartMap',
    'basePath' => $vendorDir . '/doublesecretagency/craft-smartmap/src',
    'handle' => 'smart-map',
    'aliases' => 
    array (
      '@doublesecretagency/smartmap' => $vendorDir . '/doublesecretagency/craft-smartmap/src',
    ),
    'name' => 'Smart Map',
    'version' => '3.1.0',
    'description' => 'The most comprehensive proximity search and mapping tool for Craft.',
    'developer' => 'Double Secret Agency',
    'developerUrl' => 'https://www.doublesecretagency.com/plugins',
    'documentationUrl' => 'https://www.doublesecretagency.com/plugins/smart-map/docs',
  ),
  'craftcms/query' => 
  array (
    'class' => 'craft\\query\\Plugin',
    'basePath' => $vendorDir . '/craftcms/query/src',
    'handle' => 'query',
    'aliases' => 
    array (
      '@craft/query' => $vendorDir . '/craftcms/query/src',
    ),
    'name' => 'Query',
    'version' => '2.0.2',
    'description' => 'Execute database queries from the Control Panel',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/query',
    'changelogUrl' => 'https://raw.githubusercontent.com/craftcms/query/v2/CHANGELOG.md',
    'downloadUrl' => 'https://github.com/craftcms/query/archive/v2.zip',
  ),
  'amimpact/command-palette' => 
  array (
    'class' => 'amimpact\\commandpalette\\CommandPalette',
    'basePath' => $vendorDir . '/amimpact/command-palette/src',
    'handle' => 'command-palette',
    'aliases' => 
    array (
      '@amimpact/commandpalette' => $vendorDir . '/amimpact/command-palette/src',
    ),
    'name' => 'Command Palette',
    'version' => '3.1.4',
    'schemaVersion' => '3.0.0',
    'description' => 'Command palette in Craft.',
    'developer' => 'a&m impact',
    'developerUrl' => 'http://www.am-impact.nl',
    'documentationUrl' => 'https://github.com/am-impact/amcommand/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/am-impact/amcommand/craft3/CHANGELOG.md',
    'hasCpSection' => true,
    'components' => 
    array (
      'entries' => 'amimpact\\commandpalette\\services\\Entries',
      'general' => 'amimpact\\commandpalette\\services\\General',
      'globals' => 'amimpact\\commandpalette\\services\\Globals',
      'plugins' => 'amimpact\\commandpalette\\services\\Plugins',
      'search' => 'amimpact\\commandpalette\\services\\Search',
      'settings' => 'amimpact\\commandpalette\\services\\Settings',
      'tasks' => 'amimpact\\commandpalette\\services\\Tasks',
      'utilities' => 'amimpact\\commandpalette\\services\\Utilities',
      'users' => 'amimpact\\commandpalette\\services\\Users',
    ),
  ),
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '2.6.1',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/redactor/blob/v2/README.md',
  ),
  'enupal/translate' => 
  array (
    'class' => 'enupal\\translate\\Translate',
    'basePath' => $vendorDir . '/enupal/translate/src',
    'handle' => 'enupal-translate',
    'aliases' => 
    array (
      '@enupal/translate' => $vendorDir . '/enupal/translate/src',
    ),
    'name' => 'Enupal Translate',
    'version' => '1.2.2',
    'description' => 'Translate your website templates and plugins into multiple languages. Bulk translation with Google Translate or Yandex.',
    'developer' => 'Enupal',
    'developerUrl' => 'http://enupal.com',
    'components' => 
    array (
      'app' => 'enupal\\translate\\services\\App',
    ),
  ),
  'pennebaker/craft-architect' => 
  array (
    'class' => 'pennebaker\\architect\\Architect',
    'basePath' => $vendorDir . '/pennebaker/craft-architect/src',
    'handle' => 'architect',
    'aliases' => 
    array (
      '@pennebaker/architect' => $vendorDir . '/pennebaker/craft-architect/src',
    ),
    'name' => 'Architect',
    'version' => '2.2.9',
    'schemaVersion' => '2.0.0',
    'description' => 'CraftCMS plugin to generate content models from JSON data.',
    'developer' => 'Pennebaker',
    'developerUrl' => 'https://pennebaker.com',
    'documentationUrl' => 'https://github.com/Pennebaker/craft-architect/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/Pennebaker/craft-architect/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => true,
    'components' => 
    array (
      'architectService' => 'pennebaker\\architect\\services\\ArchitectService',
    ),
  ),
  'topshelfcraft/wordsmith' => 
  array (
    'class' => 'topshelfcraft\\wordsmith\\Wordsmith',
    'basePath' => $vendorDir . '/topshelfcraft/wordsmith/src',
    'handle' => 'wordsmith',
    'aliases' => 
    array (
      '@topshelfcraft/wordsmith' => $vendorDir . '/topshelfcraft/wordsmith/src',
    ),
    'name' => 'Wordsmith',
    'version' => '3.2.0',
    'schemaVersion' => '0.0.0.0',
    'description' => '...because you have the best words.',
    'developer' => 'Michael Rog',
    'developerUrl' => 'https://topshelfcraft.com',
    'documentationUrl' => 'https://wordsmith.docs.topshelfcraft.com/',
    'changelogUrl' => 'https://raw.githubusercontent.com/topshelfcraft/wordsmith/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
      'emoji' => 'topshelfcraft\\wordsmith\\services\\EmojiService',
      'smith' => 'topshelfcraft\\wordsmith\\services\\WordsmithService',
      'typography' => 'topshelfcraft\\wordsmith\\services\\TypographyService',
    ),
  ),
  'craftcms/element-api' => 
  array (
    'class' => 'craft\\elementapi\\Plugin',
    'basePath' => $vendorDir . '/craftcms/element-api/src',
    'handle' => 'element-api',
    'aliases' => 
    array (
      '@craft/elementapi' => $vendorDir . '/craftcms/element-api/src',
    ),
    'name' => 'Element API',
    'version' => '2.6.0',
    'description' => 'Create a JSON API for your elements in Craft',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/element-api/blob/v2/README.md',
  ),
  'craftcms/feed-me' => 
  array (
    'class' => 'craft\\feedme\\Plugin',
    'basePath' => $vendorDir . '/craftcms/feed-me/src',
    'handle' => 'feed-me',
    'aliases' => 
    array (
      '@craft/feedme' => $vendorDir . '/craftcms/feed-me/src',
    ),
    'name' => 'Feed Me',
    'version' => '4.2.3',
    'description' => 'Import content from XML, RSS, CSV or JSON feeds into entries, categories, Craft Commerce products, and more.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://docs.craftcms.com/feed-me/v4/',
  ),
  'kffein/craft-bulk-edit' => 
  array (
    'class' => 'kffein\\craftbulkedit\\CraftBulkEdit',
    'basePath' => $vendorDir . '/kffein/craft-bulk-edit/src',
    'handle' => 'craft-bulk-edit',
    'aliases' => 
    array (
      '@kffein/craftbulkedit' => $vendorDir . '/kffein/craft-bulk-edit/src',
    ),
    'name' => 'Craft Bulk Edit',
    'version' => '1.0.4',
    'description' => 'Bulk edit',
    'developer' => 'KFFEIN',
    'developerUrl' => 'www.kffein.com',
    'documentationUrl' => 'https://github.com/kffein/craft-bulk-edit/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/kffein/craft-bulk-edit/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
  ),
);
