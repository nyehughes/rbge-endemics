<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;

return [
    'endpoints' => [
	  'taxonomy.json' => function() {
		return [
		    'elementType' => Entry::class,
		    'criteria' => [
			    	'section' => ['taxonomicGroups', 'taxa'], 
			    	'isASynonym' => false,
			    	'order' => 'title',
			    	'with' => [
				    	'parentTaxonomicGroup',
				    	'taxonomicRank',
				    	'genus'
			    	]
		    	],
		    	'paginate' => false,
		    	'pretty' => true,
		    	'cache' => false,
		    'transformer' => function(Entry $entry) {
			   $parentId = null;
			   $rank = null;
			   $type = null; 
			  if($entry->section->handle == 'taxa') {
			    	$parentId = isset($entry->genus[0]) ? $entry->genus[0]->id : 'NOT SET';
			    	$rank = 'Taxa';
			    	$type = 'Taxa';
			    } else { // a taxonmic group element
			    	$parentId =  isset($entry->parentTaxonomicGroup[0]) ? $entry->parentTaxonomicGroup[0]->id : "1";
			    	$rank =  isset($entry->taxonomicRank[0]) ? $entry->taxonomicRank[0]->title : null;
			    	$type = 'Group';
			    }  
			  return [
				'name' => $entry->title,
				'id' => $entry->id,
				'parentId' =>$parentId,
				'url' => $entry->url,
				'type' =>$type,
				'rank' => $rank,
				'isASynomyn' => $entry->isASynonym
				// 'jsonUrl' => UrlHelper::url("taxonomicGroups/{$entry->id}.json"),
			  ];
		    },
		];
	  },
	  // 'taxonomicGroup/<entryId:\d+>.json' => function($entryId) {
	  //     return [
	  // 	  'elementType' => Entry::class,
	  // 	  'criteria' => ['id' => $entryId],
	  // 	  'one' => true,
	  // 	  'transformer' => function(Entry $entry) {
	  // 		return [
	  //                 'title' => $entry->title,
	  //                 'url' => $entry->url
	  // 		];
	  // 	  },
	  //     ];
	  // },
    ]
];