<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 0,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),
        
        'useProjectConfigFile' => false,
        
        'aliases' => [
            '@web' => getenv('PRIMARY_SITE_URL'),
        ],
    ],

    // Dev environment settings
    'dev' => [
        // Base site URL
        //'siteUrl' => 'http://chileanendemics.test/',
        //(now being set via .env file)

        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'devMode' => true,


        // Allow backups via mamp see https://craftcms.com/support/database-backups-in-craft-3-with-mamp
            'backupCommand' => getenv('BACKUP_COMMAND'),
            'restoreCommand' => getenv('RESTORE_COMMAND'),
    ],

    // Staging environment settings
    'staging' => [
        // Base site URL
        'siteUrl' => null,
    ],

    // Production environment settings
    'production' => [
        // Base site URL
        //'siteUrl' => 'https://chileanendemics.rbge.org.uk/',
        //(now being set via .env file)

        // Dev Mode (see https://craftcms.com/support/dev-mode)
        'devMode' => false,

    ],
];
