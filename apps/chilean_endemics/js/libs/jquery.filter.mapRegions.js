var mapRegionsFilter = {
	
	$regions : null,
	active: [],
	
	init: function(){
		var self = this;
		
		self.$map = $('#Chile_Regions');
		self.$regions = self.$map.find('g.region');
		self.$regionInputs = $("fieldset[data-dimension='region']").find('input');
		self.$reset = $('#reset').parent();
		self.bindHandlers();
		return self;
	},
		
	// The "bindHandlers" method will listen for whenever a map region is clicked. 
	bindHandlers: function(){
		var self = this;
		self.$regions.on('click', function (e) {
				self.parseFilters(e);
		});
		//listen to events on the dropdown a
		self.$regionInputs.on('click', function (e) {
	 		self.parseFilters(e);
		 });
		 self.$reset.on('click', function(e){
		 	e.preventDefault();
		 	self.resetAll();
		 });
	},
	// The parseFilters needs to handle both 
	parseFilters: function(e){
		var self = this;
		var activeClassName = 'active'
		var $clicked = $(e.currentTarget);
		var $region = null;
		
		console.log($clicked);
		
		if ($clicked.prop('tagName') == "g"){ // i.e. map was clicked directly
			region = $clicked;
			// so also toggle the corresponding checkbox in the dropdowns
			var filter = $clicked.attr('data-filter');
			//console.log(self.$regionInputs);
			//console.log('[value="' + filter + '"]');  
			var $toClick = self.$regionInputs.filter('[value=".' + filter + '"]');
			//console.log($toClick); 
			$toClick.prop("checked", !$toClick.prop("checked")).trigger('change'); // toggle checked
		}
	
		
		
		// Updating map to reflect the state of the regions dropdown
		// Remove 'active' class from all map regions
		self.$regions.removeClass("active");
		
		// check the state of regions dropdown and make map relect updated changes
		var $activeRegions = self.$regionInputs.filter(':checked');
		
		$activeRegions.each(function () {
			var $t = $(this),
				filter = $t.attr('value').substr(1);
				//console.log(filter);  
			self.$regions.filter('[data-filter="' + filter + '"]')
				.addClass("active");
		});
	},
	resetAll: function() {
		var self = this;
		self.$regions.removeClass("active");
	}
}