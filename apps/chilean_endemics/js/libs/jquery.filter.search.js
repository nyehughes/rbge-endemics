// Handle filtering in the search input
var searchFilter = {

	$searchInput: null,
	$reset: null,
	outputString: '',

	init: function(){
		var self = this; 

		self.$searchInput = $('#search_input');
		self.$reset = $('#Reset');
		self.$container = $('#taxa-list');
		self.$resutsMessageContainer = $('#taxa-list').prev().find('.no-results');
		self.bindHandlers();
	},

	// The "bindHandlers" method will listen for whenever a select is changed. 

	bindHandlers: function(){
		var self = this;
		// Handle change
		self.$searchInput.on('keyup', $.debounce( 100, function(e){
			var value = $(this).val();
			self.parseSearch(value);
		}));
	},
	
	// The parseSearch method prepares the search term
	parseSearch: function(value){
		var self = this;
		dropdowns.resetAll();
		mapregions.resetAll();
		value = value.toLowerCase();
				// If the output string is empty, show all rather than none:
		if (value === '' || value.length === 1){
			self.outputString = 'all';
		}
		else {
			self.outputString = '[class*="'+value+'"]';
		}
		console.log(self.outputString); 

		// ^ we can check the console here to take a look at the filter string that is produced

		// Send the output string to MixItUp via the 'filter' method:
		
			self.filterList();
	},
	filterList: function(){
		var self = this;
		var allElements = self.$container.find('li');
		if (self.outputString == "all") { //show everything
			allElements.removeClass('hide');
			//elements.show();
			self.$resutsMessageContainer.text(translations.showing_all_msg);
		}
		else {
			var filtered = allElements.filter(self.outputString);
			
			if (filtered.length) { //some matches
				var notFiltered = allElements.not(filtered);
				filtered.show();
				filtered.removeClass('hide');
				notFiltered.addClass('hide').hide();
				
				self.$resutsMessageContainer.text(filtered.length + " " + translations.taxa_matched);

			}
			else { // no matches
				allElements.addClass('hide').delay(300).hide();;
				self.$resutsMessageContainer.text(translations.no_results_msg);
			}
		}
	}
};