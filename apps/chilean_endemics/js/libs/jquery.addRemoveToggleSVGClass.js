 // a custom function for add remove class as standard jquery on does not work on svg elements	
 (function($) {
 	$.fn.addRemoveToggleSVGClass = function(className, addOrRemove) {
 		return this.each(function() {
 			var classAttr = $(this).attr('class');
 			switch (addOrRemove) {
 				case 'remove':
 					remove ($(this), classAttr);
 					break;
 				case 'add':
 					add ($(this), classAttr);
 					break;
 				default: // toggle
 				classAttr.indexOf(className) > -1 ? 	remove ($(this), classAttr) : add ($(this), classAttr);
 
 			} // end switch
 
 		});
 
 		function add (obj, classAttr) {
 			classAttr = classAttr + (classAttr.length === 0 ? '' : ' ') + className;
 			obj.attr('class', classAttr);
 		}
 
 		function remove (obj, classAttr) {
 		classAttr = classAttr.replace(new RegExp('\\s?' + className), '');
 		obj.attr('class', classAttr);		
 		}
 	};
 }( jQuery ));