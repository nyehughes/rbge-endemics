// Search Page filtering
// To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "dropdownFilter".

var dropdownFilter = {

	// Declare any variables we will need as properties of the object
	
	resetAll: null,
	$filters: null,
	$mapfilters: null,
	$reset: null,
	$searchInput: null,
	groups: [],
	outputArray: [],
	outputString: '',

	// The "init" method will run on document ready and cache any jQuery objects we will need.

	init: function(){
		var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "dropdownFilter" object so that we can share methods and properties between all parts of the object.

		self.$filters = $('#Filters');
		self.$scrollers = $('div.scroller');
		self.$container = $('#taxa-list');
		self.fullList = self.$container.html();
		self.$resutsMessageContainer = $('#taxa-list').prev().find('.no-results');
		self.$searchInput = $('#search_input');
		self.$reset = $('#reset').parent();
		self.$filters.find('fieldset').each(function(){
				self.groups.push({
					$fieldset: $(this),
					$inputs: $(this).find('input').not('input[value=all]'),
					$all: $(this).find('input[value=all]').first(),
					dataDimension: $(this).data('dimension'),
					active: [],
					tracker: false
				});
		});
		self.bindHandlers();
		return self;
	},	
		// The "bindHandlers" method will listen for whenever a form value changes. 

		bindHandlers: function(){
			var self = this;
			
			// expand and contract the filters
			self.$filters.find('fieldset').not('.reset').on('click', function(){
				$(this).toggleClass('expanded');
				$(this).find('.scroller').removeClass('transition-ended').on('transitionend', function(e) {
				$(this).addClass('transition-ended');
				});
			});

			// dropdown filters
			self.$filters.on('change', function(e){
				self.parseFilters(e);
			});
			
			self.$reset.on('click', function(e){
				e.preventDefault();
				self.resetAll();
			});
		},

	// The parseFilters method checks which filters are active in each group and updates the interface

	parseFilters: function(e){
		var self = this;
		
		// reset the freeform search box, as this will overule its results
		self.$searchInput.val('');
	
		// loop through each filter group and add active filters to arrays
		for(var i = 0, group; group = self.groups[i]; i++){
			// reset active array
			group.active = [];
			group.tracker = 0;
			
			var $clicked = ($(e.target));
			
			// if All is pressed, check it is the all from this group then reset this group, and skip the rest this iteration of the loop
			if($clicked[0] === group.$all[0]){
				self.resetGroup(group);
				continue; // don't bother with rest of logic, just reset
			}
			
			
			// track how many inputs are checked, and update label style accordingly
			group.$inputs.each(function(){ 
				if($(this).is(':checked')) {
					group.active.push(this.value);
					$(this).parent('label').addClass('active');
					
					//if family checked, also check the corresponding genera
					 if (group.dataDimension == 'family') {
						var $genus = self.$filters.find('[data-Dimension="genus"]').find('input');//.filter
						$genus.filter('[data-parent_family="' + (this.value.substr(1)) + '"]').prop('checked', true);
						
					}
				}
				else {
					$(this).parent('label').removeClass('active');
				}
			});
			
						
			// if nothing checked or everything checked reset the group to all
			if (group.active.length == 0 || group.active.length == group.$inputs.length){
				self.resetGroup(group);
			} 
			else { //  something was checked, so flag the fieldset title, and remove active off "All" box
				group.$all.parent('label').removeClass('active');
				group.$all.prop('checked', false);
				group.$fieldset.addClass('active');
			}
			
			
			
		}
		//if any groups are filtering set active on reset button
		var activeFiltersLength = 0
		for(var i = 0, group; group = self.groups[i]; i++){
			if (group.active.length){
				activeFiltersLength ++;
			} 
		}
	
		console.log (activeFiltersLength)
	
	if (activeFiltersLength > 0) {
		self.$reset.addClass('active');
		console.log ('add active to button')
	} else {
		self.$reset.removeClass('active');
		console.log ('remove active to button')
	}

			
		
		self.concatenate();
	},
	
	// removes all the active filters from current group
	resetGroup: function(group){
		var self = this;
		
		//console.log ('reset group ran on :' + group.$fieldset.find('h4').text());

		group.tracker = 0;
		group.active = [];
		
		group.$inputs.each(function(){ // and remove all active class
				$(this).parent('label').removeClass('active');
				$(this).prop('checked', false);
		});
				
		//select "all"	box		
		group.$all.parent('label').addClass('active');
		group.$all.prop('checked', true);
		
		//remove marker from fieldset title
		group.$fieldset.removeClass('active');
		
	},
	
	resetAll: function() {
		var self = this;
		
		for(var i = 0; i < self.groups.length; i++){
			self.resetGroup(self.groups[i]);
		}
		self.$reset.removeClass('active');
		self.concatenate();
	},
	
	// The "concatenate" method will crawl through each group, concatenating filters as desired:

	concatenate: function(){
		var self = this,
			cache = '',
			crawled = false,
			checkTrackers = function(){
				var done = 0;

				for(var i = 0, group; group = self.groups[i]; i++){
					(group.tracker === false) && done++;
				}

				return (done < self.groups.length);
			},
			crawl = function(){
				for(var i = 0, group; group = self.groups[i]; i++){
					group.active[group.tracker] && (cache += group.active[group.tracker]);

					if(i === self.groups.length - 1){
						self.outputArray.push(cache);
						cache = '';
						updateTrackers();
					}
				}
			},
			updateTrackers = function(){
				for(var i = self.groups.length - 1; i > -1; i--){
					var group = self.groups[i];

					if(group.active[group.tracker + 1]){
						group.tracker++; 
						break;
					} else if(i > 0){
						group.tracker && (group.tracker = 0);
					} else {
						crawled = true;
					}
				}
			};

		self.outputArray = []; // reset output array

		do{
			crawl();
		}
		while(!crawled && checkTrackers());

		self.outputString = self.outputArray.join();

		// If the output string is empty, show all rather than none:

		!self.outputString.length && (self.outputString = 'all'); 

		console.log(self.outputString); 

		self.filterList()
			// ^ we can check the console here to take a look at the filter string that is produced

		// Send the output string to MixItUp via the 'filter' method:

		// if(self.$container.mixItUp('isLoaded')){
		// 	self.$container.mixItUp('filter', self.outputString);
		// }
		
		
	},
	
	filterList: function(){
		var self = this;
		var allElements = self.$container.find('li');
		var synomynsLength = allElements.filter('.synonym').length;
		var acceptedLength = allElements.filter('.accepted').length;
		var messageText = ""
		if (self.outputString == "all") { //show everything
			//allElements.show();
			for (var i = 0; i < allElements.length; i++) { //for loop seems much quicker than allElements.show()
				li = allElements[i];					
					if (li.style.display == "none") {
						li.style.display = "";
					}
			}
			//self.$container.html(self.fullList);
			
			//add in number of synonyms showing
			messageText = translations.showing_all_msg + " (" + acceptedLength + " " + translations.accepted + ", " + synomynsLength + " " + translations.synonyms + ")";
			
			self.$resutsMessageContainer.text(messageText);
		}
		else {
			var filtered = allElements.filter(self.outputString);
			
			if (filtered.length) { //some matches
				var notFiltered = allElements.not(filtered);
				//filtered.show();
				for (var i = 0; i < filtered.length; i++) { //for loop seems much quicker than allElements.show()
					li = filtered[i];					
						if (li.style.display == "none") {
							li.style.display = "";
						}
				}
				
				for (var i = 0; i < notFiltered.length; i++) { //for loop seems much quicker than allElements.show()
					li = notFiltered[i];					
						if (li.style.display == "") {
							li.style.display = "none";
						}
				}
				
				//filtered.removeClass('hide');
				notFiltered.hide();
				
				synomynsLength = filtered.filter('.synonym').length;
				acceptedLength = filtered.filter('.accepted').length;
				
				messageText = filtered.length + " " + translations.taxa_matched + " (" + acceptedLength + " " + translations.accepted + ", " + synomynsLength + " " + translations.synonyms + ")";

				self.$resutsMessageContainer.text(messageText);
				//setTimeout(function() { // hide once css3 animation finished
				//notFiltered.hide();
				//}, 400)
			}
			else { // no matches
				self.$resutsMessageContainer.text(translations.no_results_msg);
				for (var i = 0; i < allElements.length; i++) { //for loop seems much quicker than allElements.show()
					li = allElements[i];					
						if (li.style.display == "") {
							li.style.display = "none";
						}
				}
				//setTimeout(function() { // hide once css3 animation finished
				//allElements.hide();
				//}, 400) 
			}

		}

	}
	
};

