var D3BarChart = {

	defaults: {
		valueLabelWidth: 50, // space reserved for value labels (right)
		barHeight: 40, // height of one bar
		barLabelWidth: 80, // space reserved for bar labels
		barLabelPadding: 5, // padding between bar and bar labels (left)
		gridLabelHeight: 18, // space reserved for gridline labels
		gridChartOffset: 3, // space between start of grid and first bar
		maxBarWidth: 500, // width of the bar with the max value
		titleHeight: 40, // height of title 
		margin: {top: 20, right: 10, bottom: 20, left: 10},
		maxRange: null // leave null to calculate from max data, or pass in number to override 
	},

	init: function(data, labelColumn, valueColumn, title, $container, options) {
	 //console.log('renderGraph called')
		
		var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent object so that we can share methods and properties between all parts of the object.
		
		//merge the options with the defaults using jquery function
		if (options) {
			$.extend(self.defaults, options);
		}
		self.data = data;
	
		self.container = $container.get(0);
		self.currentDataType = valueColumn;
		self.currentTitle = title;
		var graphDiv = self.container;

		self.totalWidth = self.defaults.maxBarWidth + self.defaults.barLabelPadding + self.defaults.barLabelWidth; // total width
		
		// accessor functions 
		var barLabel = function(d) { return d[labelColumn]; };
		var barValue = function(d) { return parseFloat(d[valueColumn]); };
		
		if ( self.defaults.maxRange === null) { self.defaults.maxRange = d3.max(data, barValue); } 

		// y scale
		var yScale = d3.scale.ordinal().domain(d3.range(0, data.length)).rangeBands([0, data.length * self.defaults.barHeight]);
		var y = function(d, i) { return yScale(i); };
		var yText = function(d, i) { return y(d, i) + yScale.rangeBand() / 2; };


		var xScale = d3.scale.linear().domain([0, self.defaults.maxRange]).range([0, self.defaults.maxBarWidth]);
		
		
		
		// svg container element
		var chart = d3.select(graphDiv)
			.append("svg")
		  .attr('width', self.defaults.maxBarWidth + self.defaults.barLabelWidth + self.defaults.valueLabelWidth)
		  .attr('height', (self.defaults.gridLabelHeight + self.defaults.gridChartOffset + data.length * self.defaults.barHeight) + (self.defaults.titleHeight * 2) )
		  .attr('margin', self.defaults.margin);

		  var titleGroup = chart.append('g')
			 .attr("width", self.totalWidth)
			 .attr("height", self.defaults.titleHeight)
			 .attr('transform', 'translate(' + self.defaults.barLabelWidth + ', 0)'); 

		  titleGroup.append('text')
			 .attr("class", "titleLabel")
			 .attr("x", "0")             
			 .attr("y", (self.defaults.titleHeight))
			 .attr("text-anchor", "start")  
			 .style("font-size", "18px") 
			 .attr('height', self.defaults.barHeight)  
			 .text(title);

		  self.chart = chart;   

		// bar labels
		var labelsContainer = chart.append('g')
		  .attr('transform', 'translate(' + (self.defaults.barLabelWidth - self.defaults.barLabelPadding) + ',' + (self.defaults.gridLabelHeight + self.defaults.gridChartOffset + self.defaults.titleHeight) + ')'); 
		labelsContainer.selectAll('text').data(data).enter().append('text')
		  .attr('y', yText)
		  .attr('stroke', 'none')
		  .attr('fill', 'black')
		  .attr("dy", ".35em") // vertical-align: middle
		  .attr('text-anchor', 'end')
		  .text(barLabel);
		// bars
		var barsContainer = chart.append('g')
		  .attr('class', "barsContainer")
		  .attr('transform', 'translate(' + self.defaults.barLabelWidth + ',' + (self.defaults.gridLabelHeight + self.defaults.gridChartOffset + self.defaults.titleHeight) + ')'); 
		barsContainer.selectAll("rect").data(data).enter().append("rect")
		  .attr('y', y)
		  .attr('height', yScale.rangeBand())
		  .attr('width', function(d) { return xScale(barValue(d)); })
		  .attr('stroke', 'white')
		  .attr('fill', '#cd795a');
		// bar value labels
		barsContainer.selectAll("text").data(data).enter().append("text")
		  .attr("x", function(d) { return xScale(barValue(d)); })
		  .attr("y", yText)
		  .attr("dx", ".4em") // padding-left
		  .attr("dy", ".35em") // vertical-align: middle
		  .attr("text-anchor", "start") // text-align: right
		  .attr("fill", "black")
		  .attr("stroke", "none")
		  .text(function(d) { return d3.round(barValue(d), 2); });
		// start line
			barsContainer.append("line")
		  .attr("y1", -self.defaults.gridChartOffset)
		  .attr("y2", yScale.rangeExtent()[1] + self.defaults.gridChartOffset)
		  .style("stroke", "#000");

		return chart;
	 },
	 updateValues: function(dataType, title) {
		var self = this; 
		 // console.log('Update barchart to data type: ' + dataType);
		self.currentDataType = dataType;
		self.currentTitle = title;
		var barValue;
		var  xScale;
		 if (dataType === 'percentage') {
			  barValue = function(d) { return d3.round((parseFloat(d.totalEndemicPlants)/parseFloat(d.totalVascularPlants)*100),0);  };
			  xScale = d3.scale.linear().domain([0, 60]).range([0, self.defaults.maxBarWidth]);
		 } else {
			  barValue = function(d) { return parseFloat(d[dataType]); };
			  xScale = d3.scale.linear().domain([0,  self.defaults.maxRange]).range([0, self.defaults.maxBarWidth]);
			  // console.log(self.defaults.maxBarWidth);
		 }

		 var svg =  self.chart.transition()
						 .duration(750);

		 var titleLabel = svg.select('text.titleLabel');
		 titleLabel.text(title);

		 var rects = svg.selectAll('rect');
		 rects.attr('width', function(d) { return xScale(barValue(d)); });

		 // bar value labels
		 var labels = svg.selectAll('.barsContainer text');
		 var percentageString =  dataType === "percentage" ? "%" : "";
		 labels.text(function(d) { return (barValue(d) + percentageString); });
		 labels.attr("x", function(d) { return xScale(barValue(d)); });

	 },

	 // Define responsive behavior
	  resize: function() {
		//console.log("Window resized");
		var self = this;
		// var svg =  self.chart;
		var containerWidth =  parseInt(d3.select(self.container).style("width"));
		//console.log(containerWidth);
		self.defaults.maxBarWidth = containerWidth - (self.defaults.barLabelPadding * 2) - self.defaults.barLabelWidth;
		self.updateValues(self.currentDataType, self.currentTitle);

	 }




};
