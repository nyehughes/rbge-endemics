// @codekit-prepend quiet "libs/jquery-3.3.1.min.js"
// @codekit-prepend quiet "libs/jquery.fancybox3.js"
// @codekit-prepend quiet "libs/lodash.js"
// @codekit-prepend quiet "libs/googleMaps.infoBox.js"
// @codekit-prepend quiet "libs/googleMaps.markerClusterer.js"
// @codekit-prepend quiet "libs/jquery.mosne_maps.js"
// @codekit-prepend quiet "libs/itemsjs.js"
// @codekit-prepend quiet "libs/vuejs-paginate-2.1.0.js"
// @codekit-prepend quiet "libs/vuejs-v2.3.2.min.js"
// @codekit-prepend quiet "libs/d3.v3.min.js"
// @codekit-prepend quiet "libs/focus-visible.min.js"
// @codekit-prepend "libs/d3.barChart.js"


$(function () { // ON Document ready
	
	// responsive menu
	//$("svg.menu-icon").click(function(){
	// 	$('div.top-nav').toggleClass("opened");
	// });
	
	$("svg.menu-icon").click(function(){
		var menu = $(this)
		var topLevelNav = $('nav.top-nav > ul');
		var subNav = $('nav.top-nav ul.subsublist');
		
		if (menu.hasClass('opened')){
			menu.removeClass('opened');
			topLevelNav.removeClass('opened');
			subNav.removeClass('opened');
		} else {
			menu.addClass('opened')
			topLevelNav.addClass('opened');
		}
	});
	
	
	$("li.contains-subitems").click(function(){
		event.stopPropagation();
		$(this).find('ul').addClass("opened");
		$('nav.top-nav > ul').removeClass("opened");
	});
	
	$(".sublist-close").click(function(){
		event.stopPropagation();
		$(this).parent().removeClass("opened");
		$('nav.top-nav > ul').addClass("opened");
	});
	
	

	function hideSyns() {
		var $syns = $('.synonym');
		var $allGenera = $('.indexes .genus');
		var $synOnlyGenera = $allGenera.filter(function () {
			if ($(this).next().find('li').not('.synonym').length > 0) {
				return false;
			} else {
				return true;
			}
		});
		if ($("#filter-syns").is(':checked')) {
			$syns.removeClass('hide');
			$synOnlyGenera.removeClass('hide');
		} else {
			$syns.addClass('hide');
			$synOnlyGenera.addClass('hide');
		}
	}

	hideSyns();
	
	//hide extra images
	
	$('#show-more-images').on('click', function(){
		// console.log('Show more images');
		$('.extra-image').slideDown("slow");
		$(this).remove();
	});

	// Load via ajax taxa index page by first letter
	var $tabs = $('.alphabetical-index a');
	var $indexListContent = $('.index-list');
	// On the click event of a tab.
	$tabs.click(function (e) {
		e.preventDefault();
		showTab($(this), $indexListContent);
	});


	function showTab(tab, $indexListContent) {

		tab.siblings().removeClass("active");
		tab.addClass("active");

		$indexListContent.load(tab.attr("href"), function () {
			var count = $indexListContent.find('li').length;
			console.log('count of taxa on page is: ' + count);
			if (count < 30) {
				$indexListContent
					.parent()
					.addClass('cols-1')
					.removeClass('cols-2 cols-3');
			} else if (count > 29) {
				$indexListContent
					.parent()
					.addClass('cols-2')
					.removeClass('cols-1 cols-3');
			}


			hideSyns();



		});
	}



	$("#filter-syns")
		.change(function () {
			console.log("click");
			hideSyns();
		});


	// Language chooser
	var myUrlBits = window.location.pathname.split('/');
	var newPath = "";
	$(".lang-chooser input[type='checkbox']")
		.change(function () {

			// if on the wrong page, then change to right page! 
			if (myUrlBits[1] !== "es") { // if on an english page then
				myUrlBits.splice(1, 0, "es"); // add es to the path
			} else {
				myUrlBits.splice(1, 1); //remove es from url
			}

			newPath = myUrlBits.join('/');
			//console.log(newPath);
			window.location.pathname = newPath;
		});
		
	
	// Tabs on entry pages
	
	// tabbed content
	$('nav.tabs ul').each(function(){
	  // For each set of tabs, we want to keep track of
	  // which tab is active and its associated content
	  var $active, $content, $links = $(this).find('a');
	
	  //Filter out tabs with no content
	  // Find articles with no content
	  $links.parent().hide()
	  console.log("Links:", $links);
		$links = $links.filter(function(){
			var $content = $(this.hash);
			console.log("Content length:", $content.text().trim().length > 0);
			return ($content.text().trim().length > 0);
		})
		console.log("Filted Links:", $links);
		$links.parent().show();
	 
	  
	  // If the location.hash matches one of the links, use that as the active tab.
	  // If no match is found, use the first visible link as the initial active tab.
	  $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
	  $active.addClass('active');
	
	  $content = $($active[0].hash);
	  	  
	  // Hide the remaining content
	  $links.not($active).each(function () {
		$(this.hash).hide();
	  });
	  
	  

	
	  // Bind the click event handler
	  $(this).on('click', 'a', function(e){
		// Make the old tab inactive.
		$active.removeClass('active');
		$content.fadeOut();
	
		// Update the variables with the new link and content
		$active = $(this);
		$content = $(this.hash);
	
		// Make the tab active.
		$active.addClass('active');
		$content.fadeIn();
		
		//update the hash in the url
		history.replaceState(undefined, undefined, this.hash);
		
		
		
		//Trigger map bounds
		$("#map").trigger("bounds");
	
		// Prevent the anchor's default click action
		e.preventDefault();
	  });
	});
	

	// modal image popover with fancybox plugin

	$('[data-fancybox="gallery"]').fancybox({
		// Options will go here
		loop: true,
		openEffect: 'elastic',
		closeEffect: 'elastic',
		transitionEffect: "fade",
		transitionDuration: 2000,

		buttons: [
			"zoom",
			//"share",
			//"slideShow",
			//"fullScreen",
			//"download",
			"close"
		],
	});

	// distribution maps

	var mapOptions = {
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		panControl: false
	};

	var image = new google.maps.MarkerImage(
		'/images/interface/marker-images/image.png',
		new google.maps.Size(22, 35),
		new google.maps.Point(0, 0),
		new google.maps.Point(11, 35)
	);

	var shadow = new google.maps.MarkerImage(
		'/images/interface/marker-images/shadow.png',
		new google.maps.Size(44, 35),
		new google.maps.Point(0, 0),
		new google.maps.Point(11, 35)
	);

	var my_cat_style = {
		red: {
			icon: '/images/interface/marker-images/image_blue.png'
		},
		blue: {
			icon: '/images/interface/marker-images/image_red.png'
		},
	};

	var my_clusterOptions = {
		minimumClusterSize: 3,
		gridSize: 17,
		styles: [{
				height: 53,
				url: "/images/interface/marker-images/m1.png",
				width: 53,
				textColor: 'white'

			},
			{
				height: 56,
				url: "/images/interface/marker-images/m2.png",
				width: 56,
				textColor: 'white'
			},
			{
				height: 66,
				url: "/images/interface/marker-images/m3.png",
				width: 66,
				textColor: 'white'
			},
			{
				height: 78,
				url: "/images/interface/marker-images/m4.png",
				width: 78,
				textColor: 'white'
			},
			{
				height: 90,
				url: "/images/interface/marker-images/m5.png",
				width: 90,
				textColor: 'white'
			}
		]
	};



	$("#map")
		.mosne_map({
			elements: '.dist-point',
			clickedzoom: 14,
			markerIconObj: image,
			markerShadowObj: shadow,
			map_opt: mapOptions,
			cat_style: my_cat_style,
			clusterOptions: my_clusterOptions,
			infowindows: false, // do not use infoWindow
			infobox: true // use infobox instead
		});


	$("#reset-map").click(function (e) {
		e.preventDefault();
		$("#map").trigger("bounds");
	});


	// Truncate long Synonym lists
	var $items = $('#syn-list').children();
	if ($items.length > 10) {
		$items.hide().slice(0, 10).show();
		$('#syn-list-viewall').click(function () {
			$items.slideDown("slow"); // or .show()
			$(this).remove();
			return false;
		});
	} else {
		$('#syn-list-viewall').remove();
	}



});